This script will prompt the user to load in a TLF then input the message for which they want stripped out.  

For example, Warning 2550 can occur for all 2D cells in the model and, for larger models, can result in the TLF being unwieldy and difficult to interrogate.  A user could input 'Warning 2550' and the script will strip out all references to the message and add a line to the TLF reporting that the Warning 2550 messages have been stripped out:

"A large number of "XY: Warning 2550 -" messages have been stripped from the log file to make it easier to manage.  See https://wiki.tuflow.com/index.php?title=TUFLOW_Message_2550 to find out more about the warning message."

The stripped file will be output as a new tlf file with the suffix '_stripped'.  The script using TKinter to provide an interface for selecting TLF files.
